# TYPROTO #

Set of examples around typography and colors with a (ugly) styleguide.
From rapid prototyping to consolidation with sass.

http://fabbrucci.me/typroto/

### Who do I talk to? ###

* Frontend Developers that want to use styleguide, but also code at the speed of light
